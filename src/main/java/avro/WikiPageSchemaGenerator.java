import avro.WikiPageSchemaBuilder;
import parser.Parser;
import parser.ParserFactory;

import java.io.*;
import java.util.Arrays;

public class main {
    private static final ParserFactory parserFactory = new ParserFactory();

    private static void walkThroughFiles(final File folder){
        for (final File fileEntry : folder.listFiles()) {
            if (fileEntry.isFile()) {
                processFile(fileEntry);
            }
        }
    }

    private static void processFile(File file) {
        String line;

        try {
            BufferedReader br = new BufferedReader(new FileReader(file));
            Parser parser = parserFactory.getParser(file.getName());

            while ((line = br.readLine()) != null) {
                System.out.println(
                        Arrays.toString(parser.parse(line))
                );
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws Exception {
        //walkThroughFiles(new File(args[0]));
        WikiPageSchemaBuilder.generateSchema();
    }
}
