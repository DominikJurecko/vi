package avro;

import org.apache.avro.Schema;
import org.apache.avro.SchemaBuilder;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;

public class WikiPageSchemaBuilder {
    // Output values indices
    public static final String RESOURCE_NAME = "name";
    public static final String RESOURCE_PAGE_ID = "page_ids";
    public static final String RESOURCE_ABSTRACT = "long_abstracts";
    public static final String RESOURCE_SHORT_ABSTRACT = "short_abstracts";

    public static final String RESOURCE_ARTICLE_CATEGORIES = "article_categories";
    public static final String RESOURCE_ARTICLE_TEMPLATES = "article_templates";
    public static final String RESOURCE_INFOBOX_PROPERTIES = "raw_infobox_properties";
    public static final String RESOURCE_GEO_COORDINATES = "geo_coordinates";

    public static final String RESOURCE_PAGE_LINKS = "page_links";
    public static final String RESOURCE_FREEBASE_LINKS = "freebase_links";

    public static final String[] resourcesWithMultipleValues = {
            RESOURCE_ARTICLE_CATEGORIES,
            RESOURCE_ARTICLE_TEMPLATES,
            RESOURCE_PAGE_LINKS,
            RESOURCE_FREEBASE_LINKS,
    };

    public static final String[] resourcesWithMap = {
            RESOURCE_GEO_COORDINATES,
            RESOURCE_INFOBOX_PROPERTIES
    };

    public static void generateSchema(){
        Schema schema = SchemaBuilder.builder().record("WikiPage")
                .namespace("org.jurecko.vinf.avro")
                .fields()
                .name(RESOURCE_NAME)
                    .type()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_PAGE_ID)
                    .type()
                    .intType()
                    .noDefault()
                .name(RESOURCE_ABSTRACT)
                    .type()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_SHORT_ABSTRACT)
                    .type()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_ARTICLE_CATEGORIES)
                    .type()
                    .array()
                    .items()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_ARTICLE_TEMPLATES)
                    .type()
                    .array()
                    .items()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_INFOBOX_PROPERTIES)
                    .type()
                    .map()
                    .values()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_GEO_COORDINATES)
                    .type()
                    .map()
                    .values()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_PAGE_LINKS)
                    .type()
                    .array()
                    .items()
                    .stringType()
                    .noDefault()
                .name(RESOURCE_FREEBASE_LINKS)
                    .type()
                    .array()
                    .items()
                    .stringType()
                    .noDefault()
                .endRecord();

        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("src/main/resources/wikiPage.avsc"));
            writer.write(schema.toString());
            writer.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        /* For hadoop
        Path schemaPath = new Path("hdfs:/tmp/schema.avsc");
        FileSystem fs = schemaPath.getFileSystem(DefaultConfiguration.get());
        OutputStream out = fs.create(schemaPath);
        out.write(bytesFor(expected.toString(), Charset.forName("utf8")));
        out.close();
        */
    }

    private static File getFileFromURL(String name) {
        URL url = WikiPageSchemaBuilder.class.getClassLoader().getResource(name);
        File file = null;
        try {
            file = new File(url.toURI());
        } catch (URISyntaxException e) {
            file = new File(url.getPath());
        } finally {
            return file;
        }
    }
}