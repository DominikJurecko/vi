package parser;

import parser.exceptions.ParserNotFoundException;

public class ParserFactory {
    public Parser getParser(String filename) throws ParserNotFoundException {
        ParserType type = ParserType.getTypeByFilename(filename);

        return new Parser(type);
    }
}