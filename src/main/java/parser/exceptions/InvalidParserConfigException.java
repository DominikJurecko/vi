package parser.exceptions;

/**
 * Throwed when parser config is broken
 */
public class InvalidParserConfigException extends Exception {
    public InvalidParserConfigException(String errorMessage){
        super(errorMessage);
    }

    public InvalidParserConfigException(String errorMessage, Throwable err){
        super(errorMessage, err);
    }
}
