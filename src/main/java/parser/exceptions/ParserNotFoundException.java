package parser.exceptions;

/**
 * Throwed when any parser not found for specified dataset file
 */
public class ParserNotFoundException extends Exception {
    public ParserNotFoundException(String errorMessage){
        super(errorMessage);
    }

    public ParserNotFoundException(String errorMessage, Throwable err){
        super(errorMessage, err);
    }
}
