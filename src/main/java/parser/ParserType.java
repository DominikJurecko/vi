package parser;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import parser.exceptions.InvalidParserConfigException;
import parser.exceptions.ParserNotFoundException;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public enum ParserType {
    PAGE_ID, ABSTRACT, SHORT_ABSTRACT, ARTICLE_CATEGORIES, ARTICLE_TEMPLATES,
    INFOBOX, GEO_COORDINATES,
    PAGE_LINKS, FREEBASE_LINKS;

    private final String pattern;
    private final Pattern compiledPattern;
    private final String fileName;

    ParserType() {
        this.pattern = ConfigHelper.getConf().get(this.name()).get("pattern");
        this.compiledPattern = Pattern.compile(this.pattern);
        this.fileName = ConfigHelper.getConf().get(this.name()).get("filename");
    }

    public static ParserType getTypeByFilename(String fileName) throws ParserNotFoundException {
        // Find correct parser for given filename
        for (ParserType type : ParserType.values()) {
            if(type.getFileName().equals(fileName)){
                return type;
            }
        }

        throw new ParserNotFoundException("Parser not found for: " + fileName);
    }

    public String getPattern() {
        return pattern;
    }

    public Pattern getCompiledPattern() {
        return this.compiledPattern;
    }

    public String getFileName() {
        return fileName;
    }

    private static final class ConfigHelper {
        private static Map<String, Map<String, String>> getConf() {
            Map<String, Map<String, String>> config = new HashMap<String, Map<String, String>>();
            ObjectMapper configReader = new ObjectMapper(new YAMLFactory());
            InputStream resource = ConfigHelper.class.getClassLoader().getResourceAsStream("parserConf.yaml");

            try {
                if (resource == null){
                    throw new InvalidParserConfigException("Config file not found");
                }
                config = configReader.readValue(resource, Map.class);

                // Check if all necessary info provided in config
                for (String parserName : config.keySet()) {
                    if(!config.get(parserName).containsKey("pattern") || !config.get(parserName).containsKey("pattern")){
                        throw new InvalidParserConfigException("Some config parameter missing in " + parserName);
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            return config;
        }
    }
}
