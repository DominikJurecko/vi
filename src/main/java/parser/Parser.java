package parser;

import java.util.regex.Matcher;

public class Parser {
    private ParserType type = null;

    public Parser(ParserType type){
        this.type = type;
    }

    public String[] parse(String line){
        // Create matcher for compiled pattern according to specified parser type
        Matcher patterMatcher = type.getCompiledPattern().matcher(line);

        if(patterMatcher.find()){
            // Return resource => [property] => value
            if(patterMatcher.groupCount() > 3){
                return new String[]{patterMatcher.group(1), patterMatcher.group(2), patterMatcher.group(3), patterMatcher.group(4)};
            }

            String[] matches = new String[patterMatcher.groupCount()];
            for(int i = 0; i < patterMatcher.groupCount(); i++){
                matches[i] = patterMatcher.group(1+1);
            }
            return matches;
        } else {
            return null;
        }
    }

    public ParserType getType(){
        return this.type;
    }
}
