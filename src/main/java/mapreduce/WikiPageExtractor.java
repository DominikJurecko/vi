package mapreduce;

import org.apache.avro.mapreduce.AvroJob;
import org.apache.commons.lang.ArrayUtils;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.FileSplit;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.util.ToolRunner;
import org.jurecko.vinf.avro.WikiPage;
import parser.Parser;
import parser.ParserFactory;

import static avro.WikiPageSchemaBuilder.*;

public class DBpediaExtractor extends Configured implements Tool {
    private static final ParserFactory parserFactory = new ParserFactory();

    // Parsed data keys
    private static final String RESOURCE_KEY = "resource";
    private static final String PROPERTY_KEY = "property";
    private static final String VALUE_KEY = "value";

    public static class WikiPageMapper extends Mapper<LongWritable, Text, Text, MapWritable> {
        private MapWritable outputValue;
        private String fileName;
        private String line;

        @Override
        protected void setup(Mapper<LongWritable, Text, Text, MapWritable>.Context context)
                throws IOException, InterruptedException {
            super.setup(context);
            this.outputValue = new MapWritable();

            String fileNameWithSuffix = ((FileSplit) context.getInputSplit()).getPath().getName();
            this.fileName = fileNameWithSuffix.substring(0, fileNameWithSuffix.length() - 7);
        }

        @Override
        public void map(LongWritable key, Text value, Context context) {
            String[] parsedValues;
            this.line = value.toString();

            if(line.startsWith("#")){
                return; // Skip comments
            }

            try {
                // Use parser related to file name
                Parser parser = parserFactory.getParser(this.fileName);

                parsedValues = parser.parse(this.line);

                if(parsedValues == null){
                    return;
                }

                if(parser.getType().getFileName().equals("geo_coordinates")){
                    this.outputValue.put(new Text(PROPERTY_KEY), new Text(parsedValues[1]));
                }

                this.outputValue.put(new Text(RESOURCE_KEY), new Text(this.fileName));
                if(parsedValues.length >= 3){
                    // Property with more values
                    this.outputValue.put(new Text(PROPERTY_KEY), new Text(parsedValues[1]));

                    if(parsedValues[2] == null){
                        this.outputValue.put(new Text(VALUE_KEY), new Text(parsedValues[3]));
                    } else {
                        this.outputValue.put(new Text(VALUE_KEY), new Text(parsedValues[2]));
                    }
                } else {
                    // Single value
                    this.outputValue.put(new Text(VALUE_KEY), new Text(parsedValues[1]));
                }

                // Key (index name), Value (Map)
                context.write(new Text(parsedValues[0]), new MapWritable(this.outputValue));
            } catch (Exception e){
                e.printStackTrace();
            } finally {
                this.outputValue.clear();
            }
        }
    }

    public static class WikiPageReducer extends Reducer<Text, MapWritable,
                Text, NullWritable> {
        private String resource, property, resourceValue;
        private final Map<String, String> infoboxValues = new HashMap<>();
        private final Map<String, String> geoCoordinates = new HashMap<>();
        private final Map<String, List<String>> wikiArrayValues = new HashMap<>();


        public void reduce(Text key, Iterable<MapWritable> values, Context context)
                throws IOException, InterruptedException {
            WikiPage record = new WikiPage();
            record.put(RESOURCE_NAME, key.toString());

            // Iterate all values for specified key and fill WikiPage avro schema
            for (MapWritable value : values) {
                this.resource = value.get(new Text(RESOURCE_KEY)).toString();
                this.resourceValue = value.get(new Text(VALUE_KEY)).toString();

                // Reduce logic
                if(ArrayUtils.contains(resourcesWithMultipleValues, this.resource)){
                    this.wikiArrayValues.computeIfAbsent(resource, a->new ArrayList<>()).add(this.resourceValue);
                } else if(this.resource.equals(RESOURCE_PAGE_ID)) {
                    record.put(this.resource, Integer.valueOf(this.resourceValue));
                } else if(this.resource.equals(RESOURCE_INFOBOX_PROPERTIES)) {
                    this.property = value.get(new Text(PROPERTY_KEY)).toString();
                    infoboxValues.put(this.property, this.resourceValue);
                } else if(this.resource.equals(RESOURCE_GEO_COORDINATES)) {
                    this.property = value.get(new Text(PROPERTY_KEY)).toString();
                    geoCoordinates.put(this.property, this.resourceValue);
                } else {
                    record.put(this.resource, this.resourceValue);
                }
            }

            record.put(RESOURCE_INFOBOX_PROPERTIES, this.infoboxValues);
            record.put(RESOURCE_GEO_COORDINATES, this.geoCoordinates);
            for (Map.Entry<String, List<String>> entry : this.wikiArrayValues.entrySet()) {
                record.put(entry.getKey(), entry.getValue());
            }

            // Write WikiPage as text (json) to result file
            context.write(new Text(record.toString()), NullWritable.get());
            wikiArrayValues.clear();
            infoboxValues.clear();
            geoCoordinates.clear();
        }
    }

    @Override
    public int run(String[] args) throws Exception {
        Configuration conf = new Configuration();
        Job job = Job.getInstance(conf, "Avro-Wiki");
        job.setJarByClass(getClass());
        job.setMapperClass(WikiPageMapper.class);
        job.setReducerClass(WikiPageReducer.class);

        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(MapWritable.class);
        AvroJob.setOutputKeySchema(job,	WikiPage.getClassSchema());

        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        FileInputFormat.addInputPath(job, new Path(args[0]));

        Path outputDirectory = new Path(args[1]);
        FileOutputFormat.setOutputPath(job, outputDirectory);

        // Delete output if exists
        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(outputDirectory))
            hdfs.delete(outputDirectory, true);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static void main(String[] args) throws Exception{
        int exitFlag = ToolRunner.run(new DBpediaExtractor(), args);
        System.exit(exitFlag);
    }
}
