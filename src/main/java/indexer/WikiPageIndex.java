package indexer;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.Map;

@Document(indexName = "wiki")
public class WikiPage {
    @Id
    private String name;

    private Integer pageId;
    private String longAbstract;
    private String shortAbstract;
    private Integer inLinkCount;
    private Integer outLinkCount;
    private String label;

    private String[] categories;
    private String[] templates;
    private Map<String, String> infobox;
    private Map<String, String> geoCoordinates;

    private String[] pageLinks;
    private String[] freebaseLinks;
    private String[] externalLinks;
    private String[] wikipediaLinks;

    public static void main(String[] args) {
    }
}