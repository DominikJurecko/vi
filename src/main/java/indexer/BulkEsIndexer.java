/*
package indexer;

import java.io.IOException;
import java.text.ParseException;

public class BulkIndexer {
    public boolean bulkInsert( String indexName, String indexType, String dataPath ) throws IOException, ParseException {
        BulkRequestBuilder bulkRequest = client.prepareBulk();

        JSONParser parser = new JSONParser();
        // we know we get an array from the example data
        JSONArray jsonArray = (JSONArray) parser.parse( new FileReader( dataPath ) );

        @SuppressWarnings("unchecked")
        Iterator<JSONObject> it = jsonArray.iterator();

        while( it.hasNext() ) {
            JSONObject json = it.next();
            logger.info( "Insert document: " + json.toJSONString() );

            bulkRequest.setRefreshPolicy( RefreshPolicy.IMMEDIATE ).add(
                    client.prepareIndex( indexName, indexType )
                            .setSource( json.toJSONString(), XContentType.JSON )
            );
        }

        BulkResponse bulkResponse = bulkRequest.get();
        if ( bulkResponse.hasFailures() ) {
            logger.info( "Bulk insert failed: " + bulkResponse.buildFailureMessage() );
            return false;
        }

        return true;
    }
}
*/
